<%-- 
    Document   : index
    Created on : 08-10-2021, 10:50:42
    Author     : Sammy Guergachi <sguergachi at gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro de Gatitos</title>
    </head>
    <body>
        <h1>Registro de Gatitos</h1>
        <form name="form" action="GatitosController" method="GET">
        <img src="img/gatito.jpg" width="500" height="300"> <br>
        <button type="submit" name="accion" value="agregar" class="btn btn-success">Agregar</button>
        <button type="submit" name="accion" value="editar" class="btn btn-success">Editar</button>
        <button type="submit" name="accion" value="eliminar" class="btn btn-success">Eliminar</button>
        <button type="submit" name="accion" value="listar" class="btn btn-success">Ver Lista</button>
        </form>
    </body>
</html>
