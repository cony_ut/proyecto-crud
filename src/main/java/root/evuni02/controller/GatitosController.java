/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evuni02.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.evuni02.GatitosDAO.GatitosJpaController;
import root.evuni02.GatitosDAO.exceptions.NonexistentEntityException;
import root.evuni02.entity.Gatitos;

@WebServlet(name = "GatitosController", urlPatterns = {"/GatitosController"})
public class GatitosController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GatitosController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet GatitosController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String boton = request.getParameter("accion");
        GatitosJpaController dao = new GatitosJpaController();

        if (boton.equals("editar") || boton.equals("eliminar") || boton.equals("listar")) {
            List<Gatitos> lista = dao.findGatitosEntities();
            request.setAttribute("lista", lista);
            request.getRequestDispatcher("listar.jsp").forward(request, response);
        }

        if (boton.equals("agregar")) {
            request.getRequestDispatcher("agregar.jsp").forward(request, response);
        }

        if (boton.equals("volver")) {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }

        if (boton.equals("cambiar")) {
            String seleccion = request.getParameter("seleccion");
            Integer seleccionInt = Integer.parseInt(seleccion);
            Gatitos gat = dao.findGatitos(seleccionInt);
            request.setAttribute("gatito", gat);
            request.getRequestDispatcher("editar.jsp").forward(request, response);
        }
        
        if (boton.equals("borrar")) {
            try {
                String seleccion = request.getParameter("seleccion");
                Integer seleccionInt = Integer.parseInt(seleccion);
                dao.destroy(seleccionInt);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(GatitosController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            List<Gatitos> lista = dao.findGatitosEntities();
            request.setAttribute("lista", lista);
            request.getRequestDispatcher("listar.jsp").forward(request, response);
            
            
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String boton = request.getParameter("accion");
            GatitosJpaController dao = new GatitosJpaController();

            String id = request.getParameter("id");
            String nombre = request.getParameter("nombre");
            String raza = request.getParameter("raza");
            String edad = request.getParameter("edad");
            String genero = request.getParameter("genero");
            String vacuna = request.getParameter("vacuna");

            Integer idInt = Integer.parseInt(id);
            boolean vacunaBoo = Boolean.parseBoolean(vacuna);

            Gatitos gatito = new Gatitos();
            gatito.setId(idInt);
            gatito.setNombre(nombre);
            gatito.setRaza(raza);
            gatito.setEdad(edad);
            gatito.setGenero(genero);
            gatito.setVacuna(vacunaBoo);

            if (boton.equals("grabar")) {
                dao.create(gatito);
            }
            if (boton.equals("editar")){
                dao.edit(gatito);
            }

        } catch (Exception ex) {
            Logger.getLogger(GatitosController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
