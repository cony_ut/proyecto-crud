<%-- 
    Document   : editar
    Created on : 12-10-2021, 12:21:06
    Author     : Sammy Guergachi <sguergachi at gmail.com>
--%>

<%@page import="root.evuni02.entity.Gatitos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% 
    Gatitos gat = (Gatitos) request.getAttribute("gatito");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro Gatitos</title>
    </head>
    <body>
        <h1>Editar gatito</h1>
        <form name="form" action="GatitosController" method="POST">
        ID:
        <input name="id" id="id" value="<%= gat.getId()%>"><br>
        Nombre:
        <input name="nombre" id="nombre" value="<%= gat.getNombre() %>"><br>
        Raza:
        <input name="raza" id="raza" value="<%= gat.getRaza() %>"><br>
        Edad:
        <input name="edad" id="edad" value="<%= gat.getEdad() %>"><br>
        Genero: <br>
        <input type="radio" name="genero" id="hembra" value="hembra">
        <label for="hembra">Hembra</label> <br>
        <input type="radio" name="genero" id="macho" value="macho">
        <label for="macho">Macho</label> <br>
        
        ¿Tiene sus vacunas al día?<br>
        <input type="radio" name="vacuna" id="si" value="true">
        <label for="si">Sí</label> <br>
        <input type="radio" name="vacuna" id="no" value="false">
        <label for="no">No</label> <br>
        <button type="submit" name="accion" value="editar" class="btn btn-success">Editar</button>
        </form>
    </body>
</html>
