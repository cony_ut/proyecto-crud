

package root.evuni02.GatitosDAO;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.evuni02.GatitosDAO.exceptions.NonexistentEntityException;
import root.evuni02.GatitosDAO.exceptions.PreexistingEntityException;
import root.evuni02.entity.Gatitos;


public class GatitosJpaController implements Serializable {

    public GatitosJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Gatitos gatitos) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(gatitos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findGatitos(gatitos.getId()) != null) {
                throw new PreexistingEntityException("Gatitos " + gatitos + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Gatitos gatitos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            gatitos = em.merge(gatitos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = gatitos.getId();
                if (findGatitos(id) == null) {
                    throw new NonexistentEntityException("The gatitos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Gatitos gatitos;
            try {
                gatitos = em.getReference(Gatitos.class, id);
                gatitos.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The gatitos with id " + id + " no longer exists.", enfe);
            }
            em.remove(gatitos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Gatitos> findGatitosEntities() {
        return findGatitosEntities(true, -1, -1);
    }

    public List<Gatitos> findGatitosEntities(int maxResults, int firstResult) {
        return findGatitosEntities(false, maxResults, firstResult);
    }

    private List<Gatitos> findGatitosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Gatitos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Gatitos findGatitos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Gatitos.class, id);
        } finally {
            em.close();
        }
    }

    public int getGatitosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Gatitos> rt = cq.from(Gatitos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
