/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package root.evuni02.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Entity
@Table(name = "gatitos")
@NamedQueries({
    @NamedQuery(name = "Gatitos.findAll", query = "SELECT g FROM Gatitos g"),
    @NamedQuery(name = "Gatitos.findByRaza", query = "SELECT g FROM Gatitos g WHERE g.raza = :raza"),
    @NamedQuery(name = "Gatitos.findByNombre", query = "SELECT g FROM Gatitos g WHERE g.nombre = :nombre"),
    @NamedQuery(name = "Gatitos.findById", query = "SELECT g FROM Gatitos g WHERE g.id = :id"),
    @NamedQuery(name = "Gatitos.findByEdad", query = "SELECT g FROM Gatitos g WHERE g.edad = :edad"),
    @NamedQuery(name = "Gatitos.findByVacuna", query = "SELECT g FROM Gatitos g WHERE g.vacuna = :vacuna"),
    @NamedQuery(name = "Gatitos.findByGenero", query = "SELECT g FROM Gatitos g WHERE g.genero = :genero")})
public class Gatitos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "raza")
    private String raza;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 2147483647)
    @Column(name = "edad")
    private String edad;
    @Column(name = "vacuna")
    private Boolean vacuna;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "genero")
    private String genero;

    public Gatitos() {
    }

    public Gatitos(Integer id) {
        this.id = id;
    }

    public Gatitos(Integer id, String raza, String nombre, String genero) {
        this.id = id;
        this.raza = raza;
        this.nombre = nombre;
        this.genero = genero;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public Boolean getVacuna() {
        return vacuna;
    }

    public void setVacuna(Boolean vacuna) {
        this.vacuna = vacuna;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gatitos)) {
            return false;
        }
        Gatitos other = (Gatitos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.evuni02.entity.Gatitos[ id=" + id + " ]";
    }

}
