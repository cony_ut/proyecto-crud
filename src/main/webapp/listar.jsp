<%-- 
    Document   : listar
    Created on : 08-10-2021, 13:15:33
    Author     : Sammy Guergachi <sguergachi at gmail.com>
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.evuni02.entity.Gatitos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Gatitos> lista = (List<Gatitos>) request.getAttribute("lista");
    Iterator<Gatitos> itGatitos = lista.iterator();
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro de Gatitos</title>
    </head>
    <body>
        <h1>Lista de gatitos</h1>
        <form name="form" action="GatitosController" method="GET">
            <table border="2">
                <th> ID </th>
                <th> Nombre </th>
                <th> Edad </th>
                <th> Raza </th>
                <th> Genero </th>
                <th> Vacuna </th>
                <th>Editar / Eliminar</th>
                    <% while (itGatitos.hasNext()) {
                            Gatitos gat = itGatitos.next();%>

                <tr>
                    <td> <%= gat.getId()%> </td>
                    <td> <%= gat.getNombre()%> </td>
                    <td> <%= gat.getEdad()%> </td>
                    <td> <%= gat.getRaza()%> </td>
                    <td> <%= gat.getGenero()%> </td>
                    <td> <%= gat.getVacuna()%> </td>
                    <td> <input type="radio" name="seleccion" value="<%= gat.getId()%>"> </td>
                </tr>
                <%}%>
            </table>
            <button type="submit" name="accion" value="cambiar"> Editar registro </button>
            <button type="submit" name="accion" value="borrar"> Eliminar registro </button>
            <button type="submit" name="accion" value="volver"> Volver </button>
        </form>
    </body>
</html>
